﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.Tasks;
    using Base2art.GatedQueue.DataStorage.DbModels;
    
    public class ArchivingMessageQueueManager : MessageQueueManagerBase
    {
        public ArchivingMessageQueueManager(IDataStore dataStore)
            : base(dataStore)
        {
        }
        
        protected async override Task TruncateCompleted()
        {
            var itemsWithRetriesCreated =
                this.DataStore.Select<message_queue_message_v1>()
                .WithNoLock()
                .LeftJoin<message_queue_message_v1>((t1, t2) => t1.previous_message_id == t2.id)
                .LeftJoin<message_queue_message_v1>((t1, t2, t3) => t2.previous_message_id == t3.id)
                .Where2(rs => rs.Field(x => x.id, x => x != null))
                .Where3(rs => rs.Field(x => x.id, x => x == null))
                .Fields2(rs => rs.Field(x => x.id));
            
            await this.DataStore.Insert<message_queue_message_history_v1>()
                .Fields(rs => this.Setup(rs))
                .Records(
                    this.DataStore.Select<message_queue_message_v1>()
                    .WithNoLock()
                    .Fields(rs => this.Setup(rs))
                    .Where(r => r.FieldIn(x => x.id, itemsWithRetriesCreated)))
                .Execute();
            
            
            var successfullItems =
                this.DataStore.Select<message_queue_message_v1>()
                .WithNoLock()
                .LeftJoin<message_queue_message_v1>((t1, t2) => t1.previous_message_id == t2.id)
                .Where1(rs => rs.Field(x => x.processing_completed_successfully, true, (x, y) => x == y))
                .Where2(rs => rs.Field(x => x.id, x => x == null))
                .Fields1(rs => rs.Field(x => x.id));
            
            
            await this.DataStore.Insert<message_queue_message_history_v1>()
                
                .Fields(rs => this.Setup(rs))
                .Records(
                    this.DataStore.Select<message_queue_message_v1>()
                    .WithNoLock()
                    .Fields(rs => this.Setup(rs))
                    .Where(r => r.FieldIn(x => x.id, successfullItems)))
                .Execute();
            
            
            
            var failedItems =
                this.DataStore.Select<message_queue_message_v1>()
                .WithNoLock()
                .LeftJoin<message_queue_message_v1>((t1, t2) => t1.previous_message_id == t2.id)
                .Where1(rs => rs.Field(x => x.processing_completed_successfully, false, (x, y) => x == y))
                .Where1(rs => rs.Field(x => x.retries_remaining, 0, (x, y) => x == y))
                .Where2(rs => rs.Field(x => x.id, x => x == null))
                .Fields1(rs => rs.Field(x => x.id));
            
            await this.DataStore.Insert<message_queue_message_history_v1>()
                
                .Fields(rs => this.Setup(rs))
                .Records(
                    this.DataStore.Select<message_queue_message_v1>()
                    .WithNoLock()
                    .Fields(rs => this.Setup(rs))
                    .Where(r => r.FieldIn(x => x.id, failedItems)))
                .Execute();
            
            await this.DataStore.Delete<message_queue_message_v1>()
                .Where(r => r.FieldIn(
                    x => x.id,
                    this.DataStore.Select<message_queue_message_history_v1>().WithNoLock().Fields(r1 => r1.Field(x => x.id))))
                .Execute();
        }

        void Setup(Base2art.DataStorage.DataManipulation.Builders.IFieldListBuilder<message_queue_message_history_v1> rs)
        {
            rs
                .Field(x => x.application_data)
                .Field(x => x.application_key)
                .Field(x => x.application_type)
                .Field(x => x.approved_at)
                .Field(x => x.approver)
                .Field(x => x.id)
                .Field(x => x.previous_message_id)
                .Field(x => x.process_timeout)
                .Field(x => x.processing_begun_at)
                .Field(x => x.processing_by_machine)
                .Field(x => x.processing_by_process)
                .Field(x => x.processing_by_thread)
                .Field(x => x.processing_completed_at)
                .Field(x => x.processing_completed_details)
                .Field(x => x.processing_completed_successfully)
                .Field(x => x.processing_lock)
                .Field(x => x.requested_at)
                .Field(x => x.requestor)
                .Field(x => x.retries_remaining);
        }

        void Setup(Base2art.DataStorage.DataManipulation.Builders.IFieldListBuilder<message_queue_message_v1> rs)
        {
            rs
                .Field(x => x.application_data)
                .Field(x => x.application_key)
                .Field(x => x.application_type)
                .Field(x => x.approved_at)
                .Field(x => x.approver)
                .Field(x => x.id)
                .Field(x => x.previous_message_id)
                .Field(x => x.process_timeout)
                .Field(x => x.processing_begun_at)
                .Field(x => x.processing_by_machine)
                .Field(x => x.processing_by_process)
                .Field(x => x.processing_by_thread)
                .Field(x => x.processing_completed_at)
                .Field(x => x.processing_completed_details)
                .Field(x => x.processing_completed_successfully)
                .Field(x => x.processing_lock)
                .Field(x => x.requested_at)
                .Field(x => x.requestor)
                .Field(x => x.retries_remaining);
        }
    }
}
