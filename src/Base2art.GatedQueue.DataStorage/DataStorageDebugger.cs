﻿//
//using System;
//using System.ComponentModel;
//using System.Threading.Tasks;
//using Base2art.DataStorage;
//using Base2art.GatedQueue.DataStorage.DbModels;
//using Base2art.Tasks;
//
//namespace Base2art.GatedQueue.DataStorage
//{
//    public static class DataStorageDebugger
//    {
//        public static async Task<DebuggerComponent<T>> Verify<T>(Func<Task<T>> func)
//        {
//            return new DebuggerComponent<T>(await func(), func);
//        }
//
//        public async static Task<message_queue_message_v1[]> FindStaleItems(IDataStore db)
//        {
//            var data = await db.Select<message_queue_message_v1>()
//                .Where(r => r.Field(x => x.processing_begun_at + x.process_timeout, DateTime.UtcNow, (a, b) => a < b)
//                       .Field(x => x.processing_begun_at, (x) => x != null)
//                       .Field(x => x.processing_completed_successfully, (x) => x == null))
//                .Execute()
//                .Then()
//                .ToArray();
//            
//            return data;
//        }
//
//        public async static Task<message_queue_message_v1[]> PollMany(IDataStore db, string workItemType)
//        {
//            var data = await db.Select<message_queue_message_v1>()
//                .Where(r => r.Field(x => x.application_type, workItemType, (a, b) => a == b)
//                       .Field(x => x.processing_completed_at, x => x == null)
//                       .Field(x => x.approver, a => a != null)
//                       .Field(x => x.processing_lock, a => a == null))
//                .Execute()
//                .Then()
//                .ToArray();
//            
//            
//            return data;
//        }
//        
//        
//        public async static Task<message_queue_message_v1[]> FindFailed(IDataStore db)
//        {
//        
//            var itemsWithNoRetries =
//                db.Select<message_queue_message_v1>()
//                .LeftJoin<message_queue_message_v1>((t1, t2) => t1.id == t2.previous_message_id)
//                .Where2(rs => rs.Field(x => x.id, x => x == null))
//                .Fields1(rs => rs.Field(x => x.id));
//            
//            var data = await db.Select<message_queue_message_v1>()
//                .Fields(r =>
//                        r.Field(x => x.id)
//                        .Field(x => x.application_key)
//                        .Field(x => x.application_type)
//                        .Field(x => x.application_data)
//                        .Field(x => x.retries_remaining)
//                        .Field(x => x.process_timeout)
//                        .Field(x => x.previous_message_id)
//                        .Field(x => x.requestor)
//                        .Field(x => x.requested_at)
//                        .Field(x => x.approver)
//                        .Field(x => x.approved_at))
//                .Where(r =>
//                       r.Field(x => x.processing_completed_successfully, false, (x, y) => x == y)
//                       .Field(x => x.retries_remaining, 0, (x, y) => x > y)
//                       .FieldIn(x => x.id, itemsWithNoRetries))
//                .Execute()
//                .Then()
//                .ToArray();
//            
//            Console.WriteLine(data.Length);
//            
//            return data;
//        }
//    }
//}
