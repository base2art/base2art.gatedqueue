﻿namespace Base2art.GatedQueue.DataStorage.DbModels.Actions
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.DataDefinition;
    using Base2art.DataStorage.DataDefinition.Builders;
    using Base2art.DataStorage.Upgrade;
    
    public class CreateTables_V1 : IDataDefinitionUpgrade
    {
        public async Task Execute(IDbms dbms, DataDefinitionUpgradeProperties properties)
        {
            if (properties == null)
            {
                throw new ArgumentNullException("properties");
            }
            
            if (!properties.IsProduction)
            {
                if (properties.DbmsCanDropTables)
                {
                    await dbms.DropTable<message_queue_message_v1>().IfExists().Execute();
                    await dbms.DropTable<message_queue_message_history_v1>().IfExists().Execute();
                }
            }
            
            await dbms.CreateTable<message_queue_message_v1>()
                .Fields(Fields)
                .WithIndex("previous_message_id", rs => rs.Field(x => x.previous_message_id))
                .WithIndex("processing_lock", rs => rs.Field(x => x.processing_lock))
                .WithIndex("processing_completed_successfully", rs => rs.Field(x => x.processing_completed_successfully).Field(x => x.processing_begun_at))
                .WithIndex("id", rs => rs.Field(x => x.id))
                .WithIndex("application_key", rs => rs.Field(x => x.application_key))
                .Execute();
            
            await dbms.CreateTable<message_queue_message_history_v1>()
                .Fields(HistoryFields)
                .WithIndex("history_id", rs => rs.Field(x => x.id))
                .Execute();
        }

        static void Fields(IFieldSetupBuilder<message_queue_message_v1> r)
        {
            r.Field(x => x.application_data)
                .Field(x => x.application_key, false, Range.Create<int>(0, 256))
                .Field(x => x.application_type, false, Range.Create<int>(0, 256))
                .Field(x => x.approved_at)
                .Field(x => x.approver, false, Range.Create<int>(0, 100))
                .Field(x => x.id)
                .Field(x => x.previous_message_id)
                .Field(x => x.process_timeout)
                .Field(x => x.processing_begun_at)
                .Field(x => x.processing_by_machine, false, Range.Create<int>(0, 256))
                .Field(x => x.processing_by_process, false, Range.Create<int>(0, 256))
                .Field(x => x.processing_by_thread, false, Range.Create<int>(0, 256))
                .Field(x => x.processing_completed_at)
                .Field(x => x.processing_completed_details)
                .Field(x => x.processing_completed_successfully)
                .Field(x => x.processing_lock)
                .Field(x => x.requested_at)
                .Field(x => x.requestor, false, Range.Create<int>(0, 100))
                .Field(x => x.retries_remaining);
        }

        static void HistoryFields(IFieldSetupBuilder<message_queue_message_history_v1> r)
        {
            r.Field(x => x.application_data)
                .Field(x => x.application_key, false, Range.Create<int>(0, 256))
                .Field(x => x.application_type, false, Range.Create<int>(0, 256))
                .Field(x => x.approved_at)
                .Field(x => x.approver, false, Range.Create<int>(0, 100))
                .Field(x => x.id)
                .Field(x => x.previous_message_id)
                .Field(x => x.process_timeout)
                .Field(x => x.processing_begun_at)
                .Field(x => x.processing_by_machine, false, Range.Create<int>(0, 256))
                .Field(x => x.processing_by_process, false, Range.Create<int>(0, 256))
                .Field(x => x.processing_by_thread, false, Range.Create<int>(0, 256))
                .Field(x => x.processing_completed_at)
                .Field(x => x.processing_completed_details)
                .Field(x => x.processing_completed_successfully)
                .Field(x => x.processing_lock)
                .Field(x => x.requested_at)
                .Field(x => x.requestor, false, Range.Create<int>(0, 100))
                .Field(x => x.retries_remaining);
        }
    }
}
