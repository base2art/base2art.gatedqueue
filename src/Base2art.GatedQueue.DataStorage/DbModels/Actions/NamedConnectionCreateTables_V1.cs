﻿namespace Base2art.GatedQueue.DataStorage.DbModels.Actions
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.Upgrade;

    public class NamedConnectionCreateTables_V1 : INamedConnectionDataDefinitionUpgrade
    {
        private readonly string name;
        
        private readonly CreateTables_V1 tables = new CreateTables_V1();

        public NamedConnectionCreateTables_V1(string name)
        {
            this.name = name;
        }
        
        public Task Execute(IDbmsFactory dbmsFactory, DataDefinitionUpgradeProperties properties)
        {
            return tables.Execute(dbmsFactory.Create(this.name), properties);
        }
    }
}


