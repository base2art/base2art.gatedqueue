﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Threading.Tasks;
    
    public class DebuggerComponent<T>
    {
        private readonly T data;

        private readonly Func<Task<T>> func;

        public DebuggerComponent(T data, Func<Task<T>> func)
        {
            this.func = func;
            this.data = data;
        }

        public async Task Assert(Action<T, T> verify)
        {
            var data2 = await func();
            verify(data, data2);
            Console.WriteLine(data);
            Console.WriteLine(data2);
        }
    }
}
