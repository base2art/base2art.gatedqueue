﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.Tasks;
    using Base2art.GatedQueue.DataStorage.DbModels;
    
    public class DeletingMessageQueueManager : MessageQueueManagerBase
    {
        public DeletingMessageQueueManager(IDataStore dataStore) : base(dataStore)
        {
        }
        
        protected override Task TruncateCompleted()
        {
            var itemsWithRetriesCreated =
                this.DataStore.Select<message_queue_message_v1>()
                .WithNoLock()
                .LeftJoin<message_queue_message_v1>((t1, t2) => t1.previous_message_id == t2.id)
                .LeftJoin<message_queue_message_v1>((t1, t2, t3) => t2.previous_message_id == t3.id)
                .Where2(rs => rs.Field(x => x.id, x => x != null))
                .Where3(rs => rs.Field(x => x.id, x => x == null))
                .Fields2(rs => rs.Field(x => x.id));
            
            var successfullItems =
                this.DataStore.Select<message_queue_message_v1>()
                .WithNoLock()
                .LeftJoin<message_queue_message_v1>((t1, t2) => t1.previous_message_id == t2.id)
                .Where1(rs => rs.Field(x => x.processing_completed_successfully, true, (x, y) => x == y))
                .Where2(rs => rs.Field(x => x.id, x => x == null))
                .Fields1(rs => rs.Field(x => x.id));
            
            return this.DataStore.Delete<message_queue_message_v1>()
                .Where(r => r.FieldIn(x => x.id, itemsWithRetriesCreated))
                .Execute()
                .Then()
                .Execute(() =>
                         this.DataStore.Delete<message_queue_message_v1>()
                         .Where(r => r.FieldIn(x => x.id, successfullItems))
                         .Execute());
        }
    }
}
