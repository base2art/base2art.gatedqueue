﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Linq;
    using Base2art.DataStorage;
    using Base2art.DataStorage.DataManipulation;
    using Base2art.GatedQueue.Models;
    using Base2art.Tasks;
    using Base2art.GatedQueue.DataStorage.DbModels;

    public class MessageQueue : SimpleMessageQueueBase
    {
        private readonly IDataStore db;
        
        public MessageQueue(IDataStore dataStore)
        {
            this.db = dataStore;
        }

        public IDataStore DataStore
        {
            get { return this.db; }
        }

        internal IMessage ToMessage(message_queue_message_v1 x)
        {
            return new IndexedMessage() {
                ApplicationKey = x.application_key,
                ApprovedAt = x.approved_at.HasValue ? new DateTimeOffset(x.approved_at.Value, TimeSpan.Zero) : DateTimeOffset.MinValue,
                ApprovedBy = x.approver,
                CompletedAt = x.processing_completed_at,
                CompletedSuccessfully = x.processing_completed_successfully,
                CompletionDetails = x.processing_completed_details,
                Id = x.id,
                ParentId = (Guid?)x.previous_message_id ?? null,
                ProcessingStartedAt = x.processing_begun_at.GetValueOrDefault(),
                Processor = x.processing_by_process,
                RequestedAt = x.requested_at,
                RequestedBy = x.requestor,
                RetryCount = x.retries_remaining,
                Timeout = x.process_timeout,
                WorkItemType = x.application_type,
                WorkItemData = x.application_data,
            };
        }
        
        protected override System.Threading.Tasks.Task AddItemsToQueue(QueueWorkItemRequest[] workItems)
        {
            var inserter = this.db.Insert<message_queue_message_v1>();
            foreach (var item in workItems)
            {
                inserter.Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                .Field(x => x.application_key, item.ApplicationKey)
                                .Field(x => x.application_type, item.WorkItemType)
                                .Field(x => x.application_data, item.WorkItemData)
                                .Field(x => x.retries_remaining, item.RetryCount)
                                .Field(x => x.process_timeout, item.Timeout)
                                .Field(x => x.previous_message_id, null)
                                .Field(x => x.requestor, item.RequestedBy)
                                .Field(x => x.requested_at, item.RequestedAt.ToUniversalTime().DateTime));
                
            }
            
            return inserter.Execute();
        }
        
        protected override System.Threading.Tasks.Task AddItemsToQueueWithApproval(QueueWorkItemRequest[] workItems)
        {
            var inserter = this.db.Insert<message_queue_message_v1>();
            foreach (var item in workItems)
            {
                inserter.Record(r =>
                                           r.Field(x => x.id, Guid.NewGuid())
                                           .Field(x => x.application_key, item.ApplicationKey)
                                           .Field(x => x.application_type, item.WorkItemType)
                                           .Field(x => x.application_data, item.WorkItemData)
                                           .Field(x => x.retries_remaining, item.RetryCount)
                                           .Field(x => x.process_timeout, item.Timeout)
                                           .Field(x => x.previous_message_id, null)
                                           .Field(x => x.requestor, item.RequestedBy)
                                           .Field(x => x.requested_at, item.RequestedAt.ToUniversalTime().DateTime)
                                           .Field(x => x.approver, item.RequestedBy)
                                           .Field(x => x.approved_at, item.RequestedAt.ToUniversalTime().DateTime));
            }
            
            return inserter.Execute();
        }
        
        protected override System.Threading.Tasks.Task<IMessageRequest[]> FindUnapprovedItems(int count)
        {
            return this.db.Select<message_queue_message_v1>()
                .WithNoLock()
                .Where(r => r.Field(x => x.approver, a => a == null))
                .Limit(count)
                .Execute()
                .Then()
                .Select<IMessageRequest>(y => this.ToMessage(y))
                .Then()
                .ToArray();
        }
        
        protected override System.Threading.Tasks.Task<IMessageRequest[]> FindUnapprovedItems(string workItemType, int count)
        {
            return this.db.Select<message_queue_message_v1>()
                .WithNoLock()
                .Where(r => r.Field(x => x.approver, a => a == null)
                       .Field(x => x.application_type, workItemType, (a, b) => a == b))
                .Limit(count)
                .Execute()
                .Then()
                .Select<IMessageRequest>(y => this.ToMessage(y))
                .Then()
                .ToArray();
        }
        
        protected override System.Threading.Tasks.Task<IMessage> PeekItem(string workItemType)
        {
            return this.db.SelectSingle<message_queue_message_v1>()
                .WithNoLock()
                .Where(r => r.Field(x => x.application_type, workItemType, (a, b) => a == b)
                       .Field(x => x.approver, a => a != null)
                       .Field(x => x.processing_begun_at, x => x == null))
                .Execute()
                .Then()
                .Return<IMessage>(x => this.ToMessage(x));
        }
        
        protected async override System.Threading.Tasks.Task<IMessage> PollItem(
            string workItemType, string machine, string process, string thread)
        {
            //            await DataStorageDebugger.Verify(() => DataStorageDebugger.PollMany(this.db, workItemType));
            
            var item = await this.db.SelectSingle<message_queue_message_v1>()
                .WithNoLock()
                .Where(r => r.Field(x => x.application_type, workItemType, (a, b) => a == b)
                       .Field(x => x.processing_completed_at, x => x == null)
                       .Field(x => x.approver, a => a != null)
                       .Field(x => x.processing_lock, a => a == null))
                .Execute();
            
            if (item == null)
            {
                return null;
            }
            
            var lockId = Guid.NewGuid();
            await this.db.Update<message_queue_message_v1>()
                .Set(x => x.Field(y => y.processing_by_machine, machine)
                     .Field(y => y.processing_by_process, process)
                     .Field(y => y.processing_by_thread, thread)
                     .Field(y => y.processing_begun_at, DateTime.UtcNow)
                     .Field(y => y.processing_lock, lockId))
                .Where(r => r.Field(x => x.id, item.id, (a, b) => a == b))
                .Execute();
            
            return await this.db.SelectSingle<message_queue_message_v1>()
                .WithNoLock()
                .Where(r => r.Field(x => x.id, item.id, (a, b) => a == b)
                       .Field(x => x.processing_lock, lockId, (a, b) => a == b))
                .Execute()
                .Then()
                .Return(this.ToMessage);
        }

        
        protected override System.Threading.Tasks.Task<IMessage> GetMessageItem(Guid id)
        {
            return this.db.SelectSingle<message_queue_message_v1>()
                .WithNoLock()
                .Where(r => r.Field(x => x.id, id, (a, b) => a == b))
                .Execute()
                .Then()
                .Return(ToMessage);
        }
        
        protected override System.Threading.Tasks.Task ApproveItem(Guid id, string approver)
        {
            return this.db.Update<message_queue_message_v1>()
                .Set(x => x.Field(y => y.approver, approver)
                     .Field(y => y.approved_at, DateTime.UtcNow))
                .Where(r => r.Field(x => x.id, id, (a, b) => a == b))
                .Execute();
        }
        
        protected override System.Threading.Tasks.Task MarkItemSuccessful(Guid id, string details)
        {
            return this.db.Update<message_queue_message_v1>()
                .Set(x => x.Field(y => y.processing_completed_at, DateTime.UtcNow)
                     .Field(y => y.processing_completed_details, details)
                     .Field(y => y.processing_completed_successfully, true))
                .Where(r => r.Field(x => x.id, id, (a, b) => a == b))
                .Execute();
        }
        
        protected override System.Threading.Tasks.Task MarkItemFailed(Guid id, string details)
        {
            return this.db.Update<message_queue_message_v1>()
                .Set(x => x.Field(y => y.processing_completed_at, DateTime.UtcNow)
                     .Field(y => y.processing_completed_details, details)
                     .Field(y => y.processing_completed_successfully, false))
                .Where(r => r.Field(x => x.id, id, (a, b) => a == b))
                .Execute();
        }
    }
}
