﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.GatedQueue.Models;
    using Base2art.Tasks;
    using Base2art.GatedQueue.DataStorage.DbModels;

    public abstract class MessageQueueManagerBase : IMessageQueueManager
    {
        private readonly IDataStore db;
        
        protected MessageQueueManagerBase(IDataStore dataStore)
        {
            this.db = dataStore;
        }

        public IDataStore DataStore
        {
            get { return this.db; }
        }
        
        public virtual Task<string[]> GetJobTypes()
        {
            return this.db.Select<message_queue_message_v1>()
                .WithNoLock()
                .Fields(r => r.Field(x => x.application_type))
                .Distinct()
                .Execute()
                .Then()
                .Select(y => y.application_type)
                .Then()
                .ToArray();
        }

        public Task<long> GetQueueSize()
        {
            return this.db.SelectSingle<message_queue_message_v1>()
                .WithNoLock()
                .WithCalculated<message_counts>()
                .Fields2(rs => rs.Count(x => x.count))
                .Execute()
                .Then()
                .Return(y => y.Item2.count);
        }
        
        public async Task<IJobTypeStatus[]> GetJobTypeCounts()
        {
            var t1 = this.db.Select<message_queue_message_v1>()
                .WithNoLock()
                .Fields(r => r.Field(x => x.application_type).Field(x => x.processing_completed_successfully))
                .GroupBy<message_counts>(r => r.Field(x => x.application_type).Field(x => x.processing_completed_successfully))
                .Fields2(r => r.Count(x => x.count))
                .Where1(rs => rs.Field(x => x.processing_begun_at, x => x == null))
                .Execute()
                .Then()
                .Select<IJobTypeStatus>(y => new JobTypeStatus {
                                            State = JobState.Queued,
                                            MessageType = y.Item1.application_type,
                                            Count = (int)y.Item2.count
                                        })
                .Then()
                .ToArray();
            
            var t2 = this.db.Select<message_queue_message_v1>()
                .WithNoLock()
                .Fields(r => r.Field(x => x.application_type).Field(x => x.processing_completed_successfully))
                .GroupBy<message_counts>(r => r.Field(x => x.application_type).Field(x => x.processing_completed_successfully))
                .Fields2(r => r.Count(x => x.count))
                .Where1(rs => rs.Field(x => x.processing_begun_at, x => x != null))
                .Execute()
                .Then()
                .Select<IJobTypeStatus>(y => new JobTypeStatus {
                                            State = this.ToState(y.Item1.processing_completed_successfully.HasValue),
                                            MessageType = y.Item1.application_type,
                                            Count = (int)y.Item2.count
                                        })
                .Then()
                .ToArray();
            
            var t1Data = await t1;
            var t2Data = await t2;
            return t1Data.Union(t2Data).ToArray();
        }
        
        public virtual Task TimeoutStaleItems()
        {
            return this.db.Update<message_queue_message_v1>()
                .Set(r => r.Field(x => x.processing_completed_at, DateTime.UtcNow)
                     .Field(x => x.processing_completed_details, "TIMEOUT")
                     .Field(x => x.processing_completed_successfully, false))
                .Where(r => r.Field(x => x.processing_begun_at + x.process_timeout, DateTime.UtcNow, (a, b) => a < b)
                       .Field(x => x.processing_begun_at, (x) => x != null)
                       .Field(x => x.processing_completed_successfully, (x) => x == null))
                .Execute();
        }

        public virtual Task SetupRetries()
        {
            return this.TimeoutStaleItems().Then().Execute(() => this.AddItemsToRetry());
        }

        protected virtual Task AddItemsToRetry()
        {
            var itemsWithNoRetries =
                this.db.Select<message_queue_message_v1>()
                .WithNoLock()
                .Fields(rs => rs.Field(x => x.id))
                .LeftJoin<message_queue_message_v1>((t1, t2) => t1.id == t2.previous_message_id)
                .Where2(rs => rs.Field(x => x.id, x => x == null));
            
            
            return this.db.Insert<message_queue_message_v1>()
                .Fields(r => r.Field(x => x.id)
                        .Field(x => x.application_key)
                        .Field(x => x.application_type)
                        .Field(x => x.application_data)
                        .Field(x => x.retries_remaining)
                        .Field(x => x.process_timeout)
                        .Field(x => x.previous_message_id)
                        .Field(x => x.requestor)
                        .Field(x => x.requested_at)
                        .Field(x => x.approver)
                        .Field(x => x.approved_at))
                .Records<message_queue_message_v1>(
                    this.db.Select<message_queue_message_v1>()
                    .WithNoLock()
                    .Fields(r =>
                            r.Field(x => Guid.NewGuid())
                            .Field(x => x.application_key)
                            .Field(x => x.application_type)
                            .Field(x => x.application_data)
                            .Field(x => x.retries_remaining - 1)
                            .Field(x => x.process_timeout)
                            .Field(x => x.id)
                            .Field(x => x.requestor)
                            .Field(x => x.requested_at)
                            .Field(x => x.approver)
                            .Field(x => x.approved_at))
                    .Where(r =>
                           r.Field(x => x.processing_completed_successfully, false, (x, y) => x == y)
                           .Field(x => x.retries_remaining, 0, (x, y) => x > y)
                           .FieldIn(x => x.id, itemsWithNoRetries)))
                .Execute();
        }

        Task IMessageQueueManager.TruncateCompleted()
        {
            return this.TruncateCompleted();
        }

        private JobState ToState(bool? success)
        {
            if (!success.HasValue)
            {
                return JobState.Processing;
            }
            
            if (success.Value)
            {
                return JobState.CompletedSuccessfully;
            }
            
            return JobState.CompletedWithError;
        }
        
        protected abstract Task TruncateCompleted();
        
        private class JobTypeStatus : IJobTypeStatus
        {
            public string MessageType { get; set; }
            public int Count { get; set; }
            public JobState State { get; set; }
        }
    }
}


