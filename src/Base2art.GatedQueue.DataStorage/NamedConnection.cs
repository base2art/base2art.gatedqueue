﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Linq;
    using Base2art.DataStorage;

    public static class NamedConnection
    {
        public static IDataStore CreateDataStore(this IDataStoreFactory factory, string name)
        {
            return factory.Create(name);
        }
    }
}




