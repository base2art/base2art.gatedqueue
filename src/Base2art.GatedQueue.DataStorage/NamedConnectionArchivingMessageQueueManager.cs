﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Linq;
    using Base2art.DataStorage;

    public class NamedConnectionArchivingMessageQueueManager : ArchivingMessageQueueManager
    {
        public NamedConnectionArchivingMessageQueueManager(IDataStoreFactory factory, string name) : base(factory.CreateDataStore(name))
        {
        }
    }
}




