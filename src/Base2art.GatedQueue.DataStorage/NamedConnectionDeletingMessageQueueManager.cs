﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Linq;
    using Base2art.DataStorage;

    public class NamedConnectionDeletingMessageQueueManager : DeletingMessageQueueManager
    {
        public NamedConnectionDeletingMessageQueueManager(IDataStoreFactory factory, string name) : base(factory.CreateDataStore(name))
        {
        }
    }
}




