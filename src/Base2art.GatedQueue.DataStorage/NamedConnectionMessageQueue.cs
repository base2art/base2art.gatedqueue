﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Linq;
    using Base2art.DataStorage;

    public class NamedConnectionMessageQueue : MessageQueue
    {
        public NamedConnectionMessageQueue(IDataStoreFactory factory, string name): base(factory.CreateDataStore(name))
        {
        }
    }
}
