﻿namespace Base2art.GatedQueue.Models
{
    public interface IJobTypeStatus
    {
        string MessageType { get; }
        int Count { get; }
        JobState State { get; }
    }
    
    
}
