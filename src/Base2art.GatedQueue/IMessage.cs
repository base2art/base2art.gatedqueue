﻿namespace Base2art.GatedQueue
{
    using System;

    public interface IMessage : IMessageRequest
    {
        string ApprovedBy
        {
            get;
        }

        DateTimeOffset ApprovedAt
        {
            get;
        }
    }
}






