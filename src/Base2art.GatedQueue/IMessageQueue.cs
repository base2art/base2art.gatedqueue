﻿namespace Base2art.GatedQueue
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public interface IMessageQueue
    {
        /// <summary>
        /// Inserts the specified element into this queue
        /// </summary>
        /// <param name = "applicationKey">This is a `UNIQUE` key to ensure the same item is not processed more than one time.</param>
        /// <param name = "requestor">the person / system requesting the change</param>
        /// <param name = "retryCount">if the queuing system supports retries, this is the number of retries to be performed</param>
        /// <param name="workItemType">The type of data being inserted</param>
        /// <param name="workItemData">Data to insert.</param>
        /// <param name = "timeout">The amount of time to allow the process to run for.</param>
        /// <returns></returns>
        Task Offer(string applicationKey, string workItemType, string workItemData, string requestor, int retryCount, TimeSpan timeout);
        
        /// <summary>
        /// Inserts the specified element into this queue
        /// </summary>
        /// <param name = "requestor">the person / system requesting the change</param>
        /// <param name="workItemType">The type of data being inserted</param>
        /// <param name = "retryCount">if the queuing system supports retries, this is the number of retries to be performed</param>
        /// <param name="workItems">
        ///   A list of key value pairs where the key is a `UNIQUE` key to ensure the same item is not processed more than one time.
        ///  and the value is the data to insert
        /// </param>
        /// <param name = "timeout">The amount of time to allow the process to run for.</param>
        /// 
        /// <returns></returns>
        Task OfferMany(string workItemType, string requestor, int retryCount, TimeSpan timeout, params KeyValuePair<string, string>[] workItems);
        
        /// <summary>
        /// Inserts the specified element into this queue
        /// </summary>
        /// <param name = "applicationKey">This is a `UNIQUE` key to ensure the same item is not processed more than one time.</param>
        /// <param name = "requestorAndApprover">the person / system requesting/approving the change</param>
        /// <param name = "retryCount">if the queuing system supports retries, this is the number of retries to be performed</param>
        /// <param name="workItemType">The type of data being inserted</param>
        /// <param name="workItemData">Data to insert.</param>
        /// <param name = "timeout">The amount of time to allow the process to run for.</param>
        /// <returns></returns>
        Task OfferWithApproval(string applicationKey, string workItemType, string workItemData, string requestorAndApprover, int retryCount, TimeSpan timeout);
        
        /// <summary>
        /// Inserts the specified element into this queue
        /// </summary>
        /// <param name = "requestorAndApprover">the person / system requesting/approving the change</param>
        /// <param name="workItemType">The type of data being inserted</param>
        /// <param name = "retryCount">if the queuing system supports retries, this is the number of retries to be performed</param>
        /// <param name="workItems">
        ///   A list of key value pairs where the key is a `UNIQUE` key to ensure the same item is not processed more than one time.
        ///  and the value is the data to insert
        /// </param>
        /// <param name = "timeout">The amount of time to allow the process to run for.</param>
        /// 
        /// <returns></returns>
        Task OfferManyWithApproval(string workItemType, string requestorAndApprover, int retryCount, TimeSpan timeout, params KeyValuePair<string, string>[] workItems);
        
        /// <summary>
        /// Find unapproved Items by count
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        Task<IMessageRequest[]> FindUnapproved(int count);
        
        /// <summary>
        /// Find unapproved Items by count
        /// </summary>
        /// <param name="workItemType">The type of data being found</param>
        /// <param name="count"></param>
        /// <returns></returns>
        Task<IMessageRequest[]> FindUnapproved(string workItemType, int count);
        
        /// <summary>
        /// Returns first element or `default` without marking it as being processed;
        /// </summary>
        /// <param name="workItemType">The type of data being peeked at</param>
        /// <returns></returns>
        Task<IMessage> Peek(string workItemType);

        /// <summary>
        /// Returns first element or `default` and marks it as being processed;
        /// </summary>
        /// <param name="workItemType">The type of data being polled.</param>
        /// <param name="machine">an indicator of which machine is processing the data, for diagnostic purposes.</param>
        /// <param name="process">an indicator of which process is processing the data, for diagnostic purposes.</param>
        /// <param name="thread">an indicator of which thread is processing the data, for diagnostic purposes.</param>
        /// <returns></returns>
        Task<IMessage> Poll(string workItemType, string machine, string process, string thread);
        

        /// <summary>
        /// Gets a message regardless of status.
        /// </summary>
        /// <param name="id">the Message Id</param>
        /// <returns>The Typed message.</returns>
        Task<IMessage> GetMessage(Guid id);

        Task MarkApproved(Guid id, string approver);

        //
        //        Task<IRegisteredQueueWorkItem<T>[]> GetItems<T>(int maxCount, string processor);
        Task MarkSuccessful(Guid id);

        Task MarkSuccessful(Guid id, string details);

        Task MarkFailed(Guid id);

        Task MarkFailed(Guid id, string details);
    }
    
    
    
    
    
    //	public interface IMessageQueueManger
    //	{
    //		Task<KeyValuePair<string, int>[]> GetJobTypeCounts(string[] jobTypes, QueueWorkItemStates states);
    //
    //		Task<string[]> GetJobTypes();
    //
    //		Task<IQueueWorkItem<T>[]> PeekItems<T>(QueueWorkItemStates states);
    //
    //		Task SetupRetries();
    //
    //		Task<int> Cleanup();
    //	}
}
