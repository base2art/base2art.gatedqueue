﻿namespace Base2art.GatedQueue
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.GatedQueue.Models;
    
    public interface IMessageQueueManager
    {
        Task<long> GetQueueSize();
        
        Task<IJobTypeStatus[]> GetJobTypeCounts();
        
        Task<string[]> GetJobTypes();
        
//        Task<IQueueWorkItem<T>[]> PeekItems<T>(QueueWorkItemStates states);
        
        Task SetupRetries();
        
        Task TruncateCompleted();
    }
}
