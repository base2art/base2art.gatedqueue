﻿namespace Base2art.GatedQueue
{
    using System;

    public interface IMessageRequest
    {
        Guid Id
        {
            get ;
        }
        
        string ApplicationKey
        {
            get ;
        }
        
        string RequestedBy
        {
            get ;
        }

        DateTimeOffset RequestedAt
        {
            get ;
        }

        string WorkItemType
        {
            get;
        }

        string WorkItemData
        {
            get;
        }

        TimeSpan Timeout
        {
            get;
        }

        int RetryCount
        {
            get;
        }
    }
}




