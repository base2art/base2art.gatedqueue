﻿namespace Base2art.GatedQueue
{
    public interface IObjectSerializationService
    {
        string SerializeObject(object data);
        
        T DeserializeObject<T>(string data);
    }
}
