﻿namespace Base2art.GatedQueue
{
    using System;
    
    public interface ITypedMessage<T> : ITypedMessageRequest<T>, IMessage
    {
    }
}

