﻿namespace Base2art.GatedQueue
{
    using System;
    using System.Threading.Tasks;

    public interface ITypedMessageQueue : IMessageQueue
    {
        /// <summary>
        /// Inserts the specified element into this queue
        /// </summary>
        /// <param name = "applicationKey">This is a `UNIQUE` key to ensure the same item is not processed more than one time.</param>
        /// <param name = "requestor">the person / system requesting the change</param>
        /// <param name = "retryCount">if the queuing system supports retries, this is the number of retries to be performed</param>
        /// <param name="workItem">Data to insert.</param>
        /// <param name = "timeout">The amount of time to allow the process to run for.</param>
        /// <returns></returns>
        Task Offer<T>(string applicationKey, T workItem, string requestor, int retryCount, TimeSpan timeout);

        /// <summary>
        /// Inserts the specified element into this queue
        /// </summary>
        /// <param name = "applicationKeyGenerator">This is a `UNIQUE` key to ensure the same item is not processed more than one time.</param>
        /// <param name = "requestor">the person / system requesting the change</param>
        /// <param name = "retryCount">if the queuing system supports retries, this is the number of retries to be performed</param>
        /// <param name="workItems">Data to insert.</param>
        /// <param name = "timeout">The amount of time to allow the process to run for.</param>
        /// <returns></returns>
        Task Offer<T>(Func<T, string> applicationKeyGenerator, string requestor, int retryCount, TimeSpan timeout, params T[] workItems);
        
        /// <summary>
        /// Inserts the specified element into this queue
        /// </summary>
        /// <param name = "applicationKey">This is a `UNIQUE` key to ensure the same item is not processed more than one time.</param>
        /// <param name = "requestorAndApprover">the person / system requesting/approving the change</param>
        /// <param name = "retryCount">if the queuing system supports retries, this is the number of retries to be performed</param>
        /// <param name="workItem">Data to insert.</param>
        /// <param name = "timeout">The amount of time to allow the process to run for.</param>
        /// <returns></returns>
        Task OfferWithApproval<T>(string applicationKey, T workItem, string requestorAndApprover, int retryCount, TimeSpan timeout);

        /// <summary>
        /// Inserts the specified element into this queue
        /// </summary>
        /// <param name = "applicationKeyGenerator">This is a `UNIQUE` key to ensure the same item is not processed more than one time.</param>
        /// <param name = "requestorAndApprover">the person / system requesting/approving the change</param>
        /// <param name = "retryCount">if the queuing system supports retries, this is the number of retries to be performed</param>
        /// <param name="workItems">Data to insert.</param>
        /// <param name = "timeout">The amount of time to allow the process to run for.</param>
        /// <returns></returns>
        Task OfferManyWithApproval<T>(Func<T, string> applicationKeyGenerator, string requestorAndApprover, int retryCount, TimeSpan timeout, params T[] workItems);

        /// <summary>
        /// Returns first element or `default` without marking it as being processed;
        /// </summary>
        /// <returns></returns>
        Task<ITypedMessage<T>> Peek<T>();

        /// <summary>
        /// Returns first element or `default` and marks it as being processed;
        /// </summary>
        /// <param name="machine">an indicator of which machine is processing the data, for diagnostic purposes.</param>
        /// <param name="process">an indicator of which process is processing the data, for diagnostic purposes.</param>
        /// <param name="thread">an indicator of which thread is processing the data, for diagnostic purposes.</param>
        /// <returns></returns>
        Task<ITypedMessage<T>> Poll<T>(string machine, string process, string thread);
        
        /// <summary>
        /// Find unapproved Items by count
        /// </summary>
        /// <param name="count"></param>
        /// <returns></returns>
        Task<ITypedMessageRequest<T>[]> FindUnapproved<T>(int count);

        /// <summary>
        /// Gets a message regardless of status.
        /// </summary>
        /// <param name="id">the Message Id</param>
        /// <returns>The Typed message.</returns>
        Task<ITypedMessage<T>> GetMessage<T>(Guid id);
    }
}


