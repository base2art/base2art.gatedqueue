﻿namespace Base2art.GatedQueue
{
    public interface ITypedMessageRequest<T> : IMessageRequest
    {
        new T WorkItemData { get; }
    }
}
