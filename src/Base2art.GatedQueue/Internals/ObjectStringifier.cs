﻿namespace Base2art.GatedQueue.Internals
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    
    internal class ObjectStringifier
    {
        public string PrettyPrint(object value)
        {
            StringBuilder sb = new StringBuilder();
            HashSet<object> previouslyLogged = new HashSet<object>();
            this.WriteRootObject(sb, value, previouslyLogged);
            return sb.ToString().Trim();
        }
        
        public System.Threading.Tasks.Task<string> PrettyPrintAsync(object value)
        {
            return Task.Factory.StartNew(() => this.PrettyPrint(value));
        }
        
        
        private void WriteRootObject(StringBuilder sb, object value, HashSet<object> previouslyLogged)
        {
            if (value == null)
            {
                sb.Append("null");
                return;
            }
            
            var type = value.GetType();
            if (this.IsSimpleType(type))
            {
                var item = value.ToString();
                this.AppendString(sb, item);
                return;
            }
            
            var enumerable = value as System.Collections.IEnumerable;
            if (enumerable != null)
            {
                foreach (var item in enumerable.OfType<object>().Where(x => x != null))
                {
                    sb.Append('-');
                    sb.Append(' ');
                    this.WriteObject(sb, item, null, previouslyLogged, 0, 1, 0);
                }
                
                return;
            }
            
            var properties = type.GetProperties();
            var props = properties.Where(x => x.GetMethod != null).ToArray();
            
            for (int j = 0; j < props.Length; j++)
            {
                var child = props[j];
                
                var result = child.GetMethod.Invoke(value, new object[0]);
                this.WriteProperty(sb, child, result, previouslyLogged, 0, 0);
            }
            
            return;
        }

        private void WriteProperty(StringBuilder sb, PropertyInfo prop, object value, HashSet<object> previouslyLogged, int objectIndent, int indent)
        {
            sb.Append(' ', 2 * objectIndent);
            sb.Append(prop.Name);
            sb.Append(":");
            
            if (value == null)
            {
                if (value == null)
                {
                    sb.AppendLine(" null");
                    return;
                }
            }
            
            if (!this.IsSimpleType(prop.PropertyType))
            {
                sb.AppendLine("");
                this.WriteObject(sb, value, prop.PropertyType, previouslyLogged, indent, indent + 1, 0);
            }
            else
            {
                sb.Append(' ');
                this.WriteObject(sb, value, prop.PropertyType, previouslyLogged, 0, indent + 1, 0);
            }
        }
        
        private void WriteObject(StringBuilder sb, object value, Type valueType, HashSet<object> previouslyLogged, int currentIndent, int indent, int objectIndent)
        {
            if ((!(value  is ValueType)) && (!(value  is string)) && previouslyLogged.Contains(value))
            {
                sb.AppendLine("");
                return;
            }
            
            previouslyLogged.Add(value);
            
            sb.Append(' ', 2 * currentIndent);
            
            var type = valueType ?? value.GetType();
            if (this.IsSimpleType(type))
            {
                this.AppendString(sb, value.ToString());
                return;
            }
            
            var enumerable = value as System.Collections.IEnumerable;
            if (enumerable != null)
            {
                foreach (var item in enumerable)
                {
                    sb.Append(' ', 2 * (currentIndent + 1));
                    sb.Append("- ");
                    this.WriteObject(sb, item, null, previouslyLogged, 0, indent + 1, 0);
                }
                
                return;
            }
            
            var properties = type.GetProperties();
            var props = properties.Where(x => x.GetMethod != null).ToArray();
            
            for (int j = 0; j < props.Length; j++)
            {
                var child = props[j];
                
                object result = null;
                bool hasEx = false;
                try
                {
                    result = child.GetMethod.Invoke(value, new object[0]);
                }
                catch (TargetInvocationException)
                {
                    hasEx = true;
                }
                
                if (!hasEx)
                {
                    this.WriteProperty(sb, child, result, previouslyLogged, j == 0 ? objectIndent : indent, indent + 1);
                }
            }
            
            return;
        }

        private void AppendString(StringBuilder sb, string item)
        {
            bool hasNl = item.Contains("\n");
            if (hasNl)
            {
                sb.Append("`");
            }
            
            sb.AppendLine(item);
            
            if (hasNl)
            {
                sb.Append("`");
            }
        }
        
        private bool IsCompilerGenerated(Type t)
        {
            var attr = Attribute.GetCustomAttribute(t, typeof(System.Runtime.CompilerServices.CompilerGeneratedAttribute));
            return attr != null;
        }
        

        private bool IsSimpleType(Type type)
        {
            if (type.IsAssignableFrom(typeof(Type)))
            {
                return true;
            }
            
            if (typeof(MethodBase).IsAssignableFrom(type))
            {
                return true;
            }
            
            if (typeof(Exception).IsAssignableFrom(type))
            {
                return false;
            }
            
            if (type.IsGenericType)
            {
                return false;
            }
            
            var converter = TypeDescriptor.GetConverter(type);
            if (converter.CanConvertTo(typeof(string)) && converter.CanConvertFrom(typeof(string)))
            {
                return true;
            }
            
            if (this.IsCompilerGenerated(type))
            {
                return false;
            }
            
            var toString = ToStringMethod(type);
            if (toString != null)
            {
                return true;
            }
            
            return false;
        }
        

        private static object ToStringMethod(Type type)
        {
            if (type == null)
            {
                return null;
            }
            
            if (type == typeof(object))
            {
                return null;
            }
            
            var flags = BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public;
            
            var items = type.GetMethods(flags);
            var item = items.FirstOrDefault(x => x.Name == "ToString" && x.GetParameters().Length == 0);
            
            if (item != null)
            {
                return item;
            }
            
            return ToStringMethod(type.BaseType);
        }
    }
}

