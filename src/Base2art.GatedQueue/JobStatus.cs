﻿namespace Base2art.GatedQueue.Models
{
    public enum JobState
    {
        Queued,
        Processing,
        CompletedSuccessfully,
        CompletedWithError,
    }
}


