﻿
namespace Base2art.GatedQueue
{
    using System;
    using System.Threading.Tasks;
    using Base2art.GatedQueue.Internals;

    public static class MessageQueue
    {
        public static async Task MarkFailed(this IMessageQueue queue, Guid id, Exception ex)
        {
            if (queue == null)
            {
                throw new ArgumentNullException("queue");
            }
            
            if (ex == null)
            {
                throw new ArgumentNullException("ex");
            }
            
            var stringifier = new ObjectStringifier();
            var data = await stringifier.PrettyPrintAsync(ex);
            await queue.MarkFailed(id, data);
        }
    }
}


