﻿namespace Base2art.GatedQueue.Models
{
    using System;

    public class IndexedMessage : IMessage
    {
        public Guid Id
        {
            get;
            set;
        }

        public Guid? ParentId
        {
            get;
            set;
        }

        public string ApplicationKey
        {
            get;
            set;
        }

        public string RequestedBy
        {
            get;
            set;
        }

        public DateTimeOffset RequestedAt
        {
            get;
            set;
        }

        public string WorkItemType
        {
            get;
            set;
        }

        public string WorkItemData
        {
            get;
            set;
        }

        public string ApprovedBy
        {
            get;
            set;
        }

        public DateTimeOffset ApprovedAt
        {
            get;
            set;
        }

        public string Processor
        {
            get;
            set;
        }

        public DateTimeOffset ProcessingStartedAt
        {
            get;
            set;
        }

        public bool? CompletedSuccessfully
        {
            get;
            set;
        }

        public DateTimeOffset? CompletedAt
        {
            get;
            set;
        }

        public string CompletionDetails
        {
            get;
            set;
        }

        public int RetryCount
        {
            get;
            set;
        }

        public TimeSpan Timeout
        {
            get;
            set;
        }
    }
}


