﻿namespace Base2art.GatedQueue
{
    using System;
    using System.Diagnostics;
    using System.Threading;
    using System.Threading.Tasks;

    public static class QueueExtenders
    {
        public static Task<IMessage> Poll(this IMessageQueue queue, string workItemType)
        {
            if (queue == null)
            {
                throw new ArgumentNullException("queue");
            }
            
            var process = Process.GetCurrentProcess();
            return queue.Poll(workItemType, process.MachineName, process.ProcessName, Thread.CurrentThread.ManagedThreadId.ToString());
        }
        
        public static Task<ITypedMessage<T>> Poll<T>(this ITypedMessageQueue queue)
        {
            if (queue == null)
            {
                throw new ArgumentNullException("queue");
            }
            
            var process = Process.GetCurrentProcess();
            return queue.Poll<T>(process.MachineName, process.ProcessName, Thread.CurrentThread.ManagedThreadId.ToString());
        }
    }
}
