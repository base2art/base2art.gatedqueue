﻿namespace Base2art.GatedQueue
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.Tasks;

    public abstract class SimpleMessageQueueBase : IMessageQueue
    {
        public Task Offer(string applicationKey, string workItemType, string workItemData, string requestor, int retryCount, TimeSpan timeout)
        {
            if (string.IsNullOrWhiteSpace(requestor))
            {
                throw new ArgumentNullException("requestor", "requestor must not be null or empty");
            }
            
            if (string.IsNullOrWhiteSpace(workItemType))
            {
                throw new ArgumentNullException("workItemType", "workItemType must not be null or empty");
            }
            
            applicationKey = CorrectApplicationKey(applicationKey);
            return this.AddItemsToQueue(new[] {
                new QueueWorkItemRequest(applicationKey, workItemType, workItemData, requestor, DateTimeOffset.UtcNow, retryCount, timeout)
            });
        }

        public Task OfferMany(string workItemType, string requestor, int retryCount, TimeSpan timeout, params System.Collections.Generic.KeyValuePair<string, string>[] workItems)
        {
            if (string.IsNullOrWhiteSpace(requestor))
            {
                throw new ArgumentNullException("requestor", "requestor must not be null or empty");
            }
            
            if (string.IsNullOrWhiteSpace(workItemType))
            {
                throw new ArgumentNullException("workItemType", "workItemType must not be null or empty");
            }
            
            var items = workItems.Select(x => new SimpleMessageQueueBase.QueueWorkItemRequest(
                            this.CorrectApplicationKey(x.Key),
                            workItemType,
                            x.Value,
                            requestor,
                            DateTimeOffset.UtcNow,
                            retryCount,
                            timeout));
            
            return this.AddItemsToQueue(items.ToArray());
        }
        
        public Task OfferWithApproval(string applicationKey, string workItemType, string workItemData, string requestorAndApprover, int retryCount, TimeSpan timeout)
        {
            if (string.IsNullOrWhiteSpace(requestorAndApprover))
            {
                throw new ArgumentNullException("requestorAndApprover", "requestorAndApprover must not be null or empty");
            }
            
            if (string.IsNullOrWhiteSpace(workItemType))
            {
                throw new ArgumentNullException("workItemType", "workItemType must not be null or empty");
            }
            
            applicationKey = CorrectApplicationKey(applicationKey);
            return this.AddItemsToQueueWithApproval(new[] {
                new QueueWorkItemRequest(
                    applicationKey,
                    workItemType,
                    workItemData,
                    requestorAndApprover,
                    DateTimeOffset.UtcNow,
                    retryCount,
                    timeout)
            });
        }

        public Task OfferManyWithApproval(string workItemType, string requestorAndApprover, int retryCount, TimeSpan timeout, params System.Collections.Generic.KeyValuePair<string, string>[] workItems)
        {
            if (string.IsNullOrWhiteSpace(requestorAndApprover))
            {
                throw new ArgumentNullException("requestorAndApprover", "requestorAndApprover must not be null or empty");
            }
            
            if (string.IsNullOrWhiteSpace(workItemType))
            {
                throw new ArgumentNullException("workItemType", "workItemType must not be null or empty");
            }
            
            var items = workItems.Select(x => new SimpleMessageQueueBase.QueueWorkItemRequest(
                            this.CorrectApplicationKey(x.Key),
                            workItemType,
                            x.Value,
                            requestorAndApprover,
                            DateTimeOffset.UtcNow,
                            retryCount,
                            timeout));
            
            var itemsArray = items.ToArray();
            return this.AddItemsToQueueWithApproval(itemsArray);
        }
        
        public async Task<IMessageRequest[]> FindUnapproved(int count)
        {
            var rezult = await this.FindUnapprovedItems(count);
            if (rezult == null)
            {
                return new IMessage[0];
            }
            
            return rezult;
        }

        public Task<IMessageRequest[]> FindUnapproved(string workItemType, int count)
        {
            return this.FindUnapprovedItems(workItemType, count).Then().Coalesce(() => new IMessageRequest[0]);
        }
        
        public Task<IMessage> Peek(string workItemType)
        {
            return this.PeekItem(workItemType).Then().Return<IMessage>(x => x);
        }
        
        public Task<IMessage> Poll(string workItemType, string machine, string process, string thread)
        {
            return this.PollItem(workItemType, machine, process, thread).Then().Return<IMessage>(x => x);
        }

        public Task<IMessage> GetMessage(Guid id)
        {
            return this.GetMessageItem(id).Then().Return<IMessage>(x => x);
        }
        
        Task IMessageQueue.MarkApproved(Guid id, string approver)
        {
            if (string.IsNullOrWhiteSpace(approver))
            {
                throw new ArgumentNullException("approver", "approver must not be null or empty");
            }
            
            return this.ApproveItem(id, approver);
        }
        
        Task IMessageQueue.MarkSuccessful(Guid id)
        {
            return this.MarkSuccessful(id, null);
        }
        
        public Task MarkSuccessful(Guid id, string details)
        {
            return this.MarkItemSuccessful(id, details);
        }
        
        Task IMessageQueue.MarkFailed(Guid id)
        {
            return this.MarkFailed(id, null);
        }
        
        public Task MarkFailed(Guid id, string details)
        {
            return this.MarkItemFailed(id, details);
        }
        
        protected abstract Task AddItemsToQueue(QueueWorkItemRequest[] workItems);
        
        protected abstract Task AddItemsToQueueWithApproval(QueueWorkItemRequest[] workItems);
        
        protected abstract Task<IMessageRequest[]> FindUnapprovedItems(int count);
        
        protected abstract Task<IMessageRequest[]> FindUnapprovedItems(string workItemType, int count);
        
        protected abstract Task<IMessage> PeekItem(string workItemType);
        
        protected abstract Task<IMessage> PollItem(string workItemType, string machine, string process, string thread);
        
        protected abstract Task<IMessage> GetMessageItem(Guid id);

        protected abstract Task ApproveItem(Guid id, string approver);

        protected abstract Task MarkItemSuccessful(Guid id, string details);

        protected abstract Task MarkItemFailed(Guid id, string details);

        protected class QueueWorkItemRequest
        {
            private readonly string applicationKey;

            private readonly string workItemType;

            private readonly string workItemData;

            private readonly string requestedBy;

            private readonly DateTimeOffset requestedAt;

            private readonly int retryCount;

            private readonly TimeSpan timeout;
            
            public QueueWorkItemRequest(
                string applicationKey,
                string workItemType,
                string workItemData,
                string requestedBy,
                DateTimeOffset requestedAt,
                int retryCount,
                TimeSpan timeout)
            {
                this.timeout = timeout;
                this.applicationKey = applicationKey;
                this.requestedBy = requestedBy;
                this.requestedAt = requestedAt;
                this.retryCount = retryCount;
                this.workItemType = workItemType;
                this.workItemData = workItemData;
            }

            public string ApplicationKey
            {
                get { return this.applicationKey; }
            }

            public string RequestedBy
            {
                get { return this.requestedBy; }
            }

            public DateTimeOffset RequestedAt
            {
                get { return this.requestedAt; }
            }

            public int RetryCount
            {
                get { return this.retryCount; }
            }

            public TimeSpan Timeout
            {
                get { return this.timeout; }
            }

            public string WorkItemType
            {
                get { return this.workItemType; }
            }

            public string WorkItemData
            {
                get { return this.workItemData; }
            }
        }
        
        private string CorrectApplicationKey(string applicationKey)
        {
            if (string.IsNullOrWhiteSpace(applicationKey))
            {
                return Guid.NewGuid().ToString("N");
            }
            
            return applicationKey;
        }
    }
}
