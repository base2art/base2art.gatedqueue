﻿namespace Base2art.GatedQueue
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.Tasks;

    public class TypedMessageQueue : ITypedMessageQueue
    {
        private readonly IObjectSerializationService serializer;

        private readonly IMessageQueue queue;

        public TypedMessageQueue(IMessageQueue queue, IObjectSerializationService serializer)
        {
            this.serializer = serializer;
            this.queue = queue;
        }

        protected IMessageQueue Queue
        {
            get { return this.queue; }
        }
        
        public Task Offer(string applicationKey, string workItemType, string workItemData, string requestor, int retryCount, TimeSpan timeout)
        {
            return this.queue.Offer(applicationKey, workItemType, workItemData, requestor, retryCount, timeout);
        }

        public Task OfferMany(string workItemType, string requestor, int retryCount, TimeSpan timeout, params KeyValuePair<string, string>[] workItems)
        {
            return this.queue.OfferMany(workItemType, requestor, retryCount, timeout, workItems);
        }

        public Task OfferWithApproval(string applicationKey, string workItemType, string workItemData, string requestorAndApprover, int retryCount, TimeSpan timeout)
        {
            return this.queue.OfferWithApproval(applicationKey, workItemType, workItemData, requestorAndApprover, retryCount, timeout);
        }
        
        public Task OfferManyWithApproval(string workItemType, string requestorAndApprover, int retryCount, TimeSpan timeout, params KeyValuePair<string, string>[] workItems)
        {
            return this.queue.OfferManyWithApproval(workItemType, requestorAndApprover, retryCount, timeout, workItems);
        }
        
        public Task<IMessageRequest[]> FindUnapproved(int count)
        {
            return this.queue.FindUnapproved(count);
        }

        public Task<IMessageRequest[]> FindUnapproved(string workItemType, int count)
        {
            return this.queue.FindUnapproved(workItemType, count);
        }

        public Task<IMessage> Peek(string workItemType)
        {
            return this.queue.Peek(workItemType);
        }

        public Task<IMessage> Poll(string workItemType, string machine, string process, string thread)
        {
            return this.queue.Poll(workItemType, machine, process, thread);
        }

        public Task<IMessage> GetMessage(Guid id)
        {
            return this.queue.GetMessage(id);
        }

        public Task MarkApproved(Guid id, string approver)
        {
            return this.queue.MarkApproved(id, approver);
        }

        public Task MarkSuccessful(Guid id)
        {
            return this.queue.MarkSuccessful(id);
        }

        public Task MarkSuccessful(Guid id, string details)
        {
            return this.queue.MarkSuccessful(id, details);
        }

        public Task MarkFailed(Guid id)
        {
            return this.queue.MarkFailed(id);
        }

        public Task MarkFailed(Guid id, string details)
        {
            return this.queue.MarkFailed(id, details);
        }

        Task ITypedMessageQueue.Offer<T>(string applicationKey, T workItem, string requestor, int retryCount, TimeSpan timeout)
        {
            return this.Offer(applicationKey, typeof(T).FullName, this.Serialize(workItem), requestor, retryCount, timeout);
        }

        Task ITypedMessageQueue.Offer<T>(Func<T, string> applicationKeyGenerator, string requestor, int retryCount, TimeSpan timeout, params T[] workItems)
        {
            if (workItems == null)
            {
                return Task.FromResult(true);
            }
            
            workItems = workItems.Where(x => x != null).ToArray();
            if (workItems.Length == 0)
            {
                return Task.FromResult(true);
            }
            
            if (applicationKeyGenerator == null)
            {
                throw new ArgumentNullException("applicationKeyGenerator", "applicationKeyGenerator must not be null or empty");
            }
            
            var workItemType = typeof(T).FullName;
            var items = workItems.Select(x => new KeyValuePair<string, string>(applicationKeyGenerator(x), this.Serialize(x)));
            return this.OfferMany(workItemType, requestor, retryCount, timeout, items.ToArray());
        }

        Task ITypedMessageQueue.OfferWithApproval<T>(string applicationKey, T workItem, string requestorAndApprover, int retryCount, TimeSpan timeout)
        {
            return this.OfferWithApproval(applicationKey, typeof(T).FullName, this.Serialize(workItem), requestorAndApprover, retryCount, timeout);
        }

        Task ITypedMessageQueue.OfferManyWithApproval<T>(Func<T, string> applicationKeyGenerator, string requestorAndApprover, int retryCount, TimeSpan timeout, params T[] workItems)
        {
            if (workItems == null)
            {
                return Task.FromResult(true);
            }
            
            workItems = workItems.Where(x => x != null).ToArray();
            if (workItems.Length == 0)
            {
                return Task.FromResult(true);
            }
            
            if (applicationKeyGenerator == null)
            {
                throw new ArgumentNullException("applicationKeyGenerator", "applicationKeyGenerator must not be null or empty");
            }
            
            var workItemType = typeof(T).FullName;
            var items = workItems.Select(x => new KeyValuePair<string, string>(applicationKeyGenerator(x), this.Serialize(x)));
            return this.OfferManyWithApproval(workItemType, requestorAndApprover, retryCount, timeout, items.ToArray());
        }

        Task<ITypedMessageRequest<T>[]> ITypedMessageQueue.FindUnapproved<T>(int count)
        {
            var workItemType = typeof(T).FullName;
            return this.FindUnapproved(workItemType, count)
                .Then()
                .Select<ITypedMessageRequest<T>>(y => new TypedMessage<T>(y, this.Deserialize<T>(y.WorkItemData)))
                .Then()
                .ToArray();
        }

        Task<ITypedMessage<T>> ITypedMessageQueue.Peek<T>()
        {
            return this.Peek(typeof(T).FullName)
                .Then()
                .Return<ITypedMessage<T>>(x => new TypedMessage<T>(x, this.Deserialize<T>(x.WorkItemData)));
        }

        Task<ITypedMessage<T>> ITypedMessageQueue.Poll<T>(string machine, string process, string thread)
        {
            return this.Poll(typeof(T).FullName, machine, process, thread)
                .Then()
                .Return<ITypedMessage<T>>(x => new TypedMessage<T>(x, this.Deserialize<T>(x.WorkItemData)));
        }

        Task<ITypedMessage<T>> ITypedMessageQueue.GetMessage<T>(Guid id)
        {
            return this.GetMessage(id)
                .Then()
                .Return<ITypedMessage<T>>(x => new TypedMessage<T>(x, this.Deserialize<T>(x.WorkItemData)));
        }
        
        private string Serialize<T>(T workItem)
        {
            // disable once CompareNonConstrainedGenericWithNull
            return workItem == null ? null : this.serializer.SerializeObject(workItem);
        }

        private T Deserialize<T>(string data)
        {
            // disable once CompareNonConstrainedGenericWithNull
            return string.IsNullOrWhiteSpace(data) ? default(T) : this.serializer.DeserializeObject<T>(data);
        }

        private class TypedMessage<T> : ITypedMessage<T>
        {
            public TypedMessage(IMessageRequest x, T data)
            {
                this.Id = x.Id;
                this.ApplicationKey = x.ApplicationKey;
                this.WorkItemData = data;
                this.RequestedBy = x.RequestedBy;
                this.RequestedAt = x.RequestedAt;
                this.WorkItemType = x.WorkItemType;
                this.BadWorkItemData = x.WorkItemData;
                this.RetryCount = x.RetryCount;
                this.Timeout = x.Timeout;
            }

            public TypedMessage(IMessage x, T data)
            {
                this.Id = x.Id;
                this.ApplicationKey = x.ApplicationKey;
                this.WorkItemData = data;
                this.RequestedBy = x.RequestedBy;
                this.RequestedAt = x.RequestedAt;
                this.WorkItemType = x.WorkItemType;
                this.BadWorkItemData = x.WorkItemData;
                this.ApprovedAt = x.ApprovedAt;
                this.ApprovedBy = x.ApprovedBy;
                this.RetryCount = x.RetryCount;
                this.Timeout = x.Timeout;
            }

            public Guid Id
            {
                get;
                set;
            }

            public string ApplicationKey
            {
                get;
                set;
            }

            public string RequestedBy
            {
                get;
                set;
            }

            public DateTimeOffset RequestedAt
            {
                get;
                set;
            }

            public string ApprovedBy
            {
                get;
                set;
            }

            public DateTimeOffset ApprovedAt
            {
                get;
                set;
            }

            public string WorkItemType
            {
                get;
                set;
            }

            public string BadWorkItemData
            {
                get;
                set;
            }

            public T WorkItemData
            {
                get;
                set;
            }

            public int RetryCount { get; set; }

            public TimeSpan Timeout { get; set; }

            string IMessageRequest.WorkItemData
            {
                get
                {
                    return this.BadWorkItemData;
                }
            }
        }
    }
}


