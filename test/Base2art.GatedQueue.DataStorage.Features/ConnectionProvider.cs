﻿using System;
using System.Data.SqlServerCe;
using System.IO;
using System.Net;
using System.Net.Sockets;
using Base2art.Dapper;
using Base2art.DataStorage;
using Base2art.DataStorage.Dapper;
using Base2art.DataStorage.DataDefinition;
using Base2art.DataStorage.DataManipulation;
using Base2art.DataStorage.MySql;
using Base2art.DataStorage.SqlCe;
using Base2art.DataStorage.SqlServer;
using NUnit.Framework;
namespace Base2art.GatedQueue.DataStorage
{
    public static class ConnectionProvider
    {
        
        private static readonly Lazy<string> ipAddress;
        
        static ConnectionProvider()
        {
            ipAddress = new Lazy<string>(GetLocalIPAddress);
        }
        
        public static string IPAddress()
        {
            return ipAddress.Value;
        }
        
        public static Tuple<Dbms, DataStore, bool> GetStore(SupportedDatabase dbmsType)
        {
            var store = GetStoreInternal(dbmsType);
            
            return Tuple.Create(new Dbms(store.Item1), new DataStore(store.Item2), store.Item3);
        }
        
        public static Tuple<IDataDefiner, IDataManipulator, bool> GetStoreInternal(SupportedDatabase dbmsType)
        {
            if (dbmsType == SupportedDatabase.MySql)
            {
                return MySql();
            }
            
            if (dbmsType == SupportedDatabase.InMemory)
            {
//                Assert.Ignore();
                return InMemory();
            }
            
            if (dbmsType == SupportedDatabase.SqlServer)
            {
                //                if (Environment.OSVersion.Version.ToString() == "6.1.7601.65536" && !Environment.Is64BitOperatingSystem)
                //                {
                //                    Assert.Ignore();
                //                }
                
                return SqlServer();
            }
            
            if (dbmsType == SupportedDatabase.SqlCe)
            {
                //                Assert.Ignore();
                return SqlCe();
            }
            
            throw new NotImplementedException();
        }

        static Tuple<IDataDefiner, IDataManipulator, bool> MySql()
        {
            var cstr = IPAddress() == "10.0.2.15"
                ? "user id=nunit;database=nunit;password=nunitUser!;server=10.0.2.2;pooling=True"
                : "user id=nunit;database=nunit;password=nunitUser!;server=127.0.0.1;pooling=True";
            
            var factory = new OdbcConnectionFactory<MySql.Data.MySqlClient.MySqlConnection>(cstr);
            var formatter = new MySqlFormatter(new DapperExecutionEngine(factory), true, TimeSpan.FromSeconds(20));
            return Tuple.Create<IDataDefiner, IDataManipulator, bool>(formatter, formatter, true);
        }

        static Tuple<IDataDefiner, IDataManipulator, bool> SqlServer()
        {
            var cstr = SqlServerConnectionString();
            
            var factory = new OdbcConnectionFactory<System.Data.SqlClient.SqlConnection>(cstr);
            var formatter = new SqlFormatter(new DapperExecutionEngine(factory), true, "dbo", TimeSpan.FromSeconds(20));
            return Tuple.Create<IDataDefiner, IDataManipulator, bool>(formatter, formatter, true);
        }

        private static Tuple<IDataDefiner, IDataManipulator, bool> SqlCe()
        {
            var builder = new SqlCeConnectionStringBuilder();
            builder.Password = "nunitUser!";
            builder.DataSource = "NunitQueue.sdf";
            
            if (File.Exists(builder.DataSource))
            {
                File.Delete(builder.DataSource);
            }
            
            
            var cstr = builder.ToString();
            
            SqlCeEngine engine = new SqlCeEngine(cstr);
            engine.CreateDatabase();
            var factory = new OdbcConnectionFactory<SqlCeConnection>(cstr);
            var formatter = new SqlCeFormatter(new DapperExecutionEngine(factory), true);
            return Tuple.Create<IDataDefiner, IDataManipulator, bool>(formatter, formatter, false);
        }

        static Tuple<IDataDefiner, IDataManipulator, bool> InMemory()
        {
            var formatter = new Base2art.DataStorage.InMemory.DataDefiner(true);
            return Tuple.Create<IDataDefiner, IDataManipulator, bool>(formatter, formatter, true);
        }
        
        
        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            
            throw new Exception("Local IP Address Not Found!");
        }

        private static string SqlServerConnectionString()
        {
            
            var path = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "base2art",
                "gated-queue",
                "connectionString-sqlserver.config");
            
            if (File.Exists(path))
            {
                return File.ReadAllText(path).Replace("\r", "\n").Replace("\n", "");
            }
            else
            {
                var builder = new System.Data.SqlClient.SqlConnectionStringBuilder();
                builder.InitialCatalog = "nunit";
                builder.DataSource = ".";
                builder.Pooling = true;
                builder.IntegratedSecurity = true;
                
                return builder.ToString();
            }
        }
    }
}
