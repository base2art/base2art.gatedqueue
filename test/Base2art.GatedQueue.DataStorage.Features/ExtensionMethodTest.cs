﻿
namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class ExtensionMethodTest
    {
        [Test]
        public void ShouldThrowNulls()
        {
            Assert.ThrowsAsync<ArgumentNullException>(() => Base2art.GatedQueue.MessageQueue.MarkFailed(null, Guid.NewGuid(), new Exception()));
            var inner = new Mock<IMessageQueue>().Object;
            
            Assert.ThrowsAsync<ArgumentNullException>(() => Base2art.GatedQueue.MessageQueue.MarkFailed(inner, Guid.NewGuid(), null));
        }
    }
}
