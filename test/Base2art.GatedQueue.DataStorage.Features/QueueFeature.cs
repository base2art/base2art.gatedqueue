﻿namespace Base2art.GatedQueue.DataStorage
{
    using Base2art.DataStorage;
    using Base2art.GatedQueue.DataStorage.Specs;
    using NUnit.Framework;

    [TestFixture]
    public class QueueFeature
    {
        private readonly QueueSpec spec = new QueueSpec();

        
        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldOffer2Times(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldOffer2Times(store.Item1, store.Item2, store.Item3);
        }

        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldOfferDuplicateApproved(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldOfferDuplicateApproved(store.Item1, store.Item2, store.Item3);
        }

        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldDoAsync(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldDoAsync(store.Item1, store.Item2, store.Item3);
        }

        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldComplete_Failed(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldComplete_Failed(store.Item1, store.Item2, store.Item3);
        }

        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldComplete_Success(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldComplete_Success(store.Item1, store.Item2, store.Item3);
        }

        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldLoad(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldLoad(store.Item1, store.Item2, store.Item3);
        }

        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldDoubleMarkApproved_ThrowsException(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldDoubleMarkApproved_ThrowsException(store.Item1, store.Item2, store.Item3);
        }
    }
}


