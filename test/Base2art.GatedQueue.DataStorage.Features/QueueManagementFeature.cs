﻿namespace Base2art.GatedQueue.DataStorage
{
    using Base2art.DataStorage;
    using Base2art.GatedQueue.DataStorage.Specs;
    using NUnit.Framework;
    
    [TestFixture]
    public class QueueManagementFeature
    {
        private readonly QueueManagementSpec spec = new QueueManagementSpec(false);
        
        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldAcknowledgeExpired(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldAcknowledgeExpired(store.Item1, store.Item2, store.Item3);
        }
        
        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldTruncateCompleted_Failure(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldTruncateCompleted_Failure(store.Item1, store.Item2, store.Item3);
        }
        
        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldTruncateCompleted_Failure_Retries(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldTruncateCompleted_Failure_Retries(store.Item1, store.Item2, store.Item3, 1, 1);
        }
        
        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldTruncateCompleted_Success(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldTruncateCompleted_Success(store.Item1, store.Item2, store.Item3);
        }
        
        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldGetQueueSize(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldGetQueueSize(store.Item1, store.Item2, store.Item3);
        }
        
        
        [TestCase(SupportedDatabase.MySql)]
        [TestCase(SupportedDatabase.InMemory)]
        [TestCase(SupportedDatabase.SqlServer)]
        [TestCase(SupportedDatabase.SqlCe)]
        public async void ShouldGetTypeCounts(SupportedDatabase dbmsType)
        {
            var store = ConnectionProvider.GetStore(dbmsType);
            await spec.ShouldGetTypeCounts(store.Item1, store.Item2, store.Item3);
        }
    }
}
