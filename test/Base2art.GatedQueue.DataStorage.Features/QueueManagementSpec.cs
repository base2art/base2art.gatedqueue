﻿
namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Threading.Tasks;
    using FluentAssertions;
    using NUnit.Framework;

    public class QueueManagementSpec
    {
        protected virtual IMessageQueue CreateQueue()
        {
            return new MessageQueue();
        }
        
        protected virtual ITypedMessageQueue CreateTypedQueue(IMessageQueue queue)
        {
            return new TypedMessageQueue(queue, new TestSerializer());
        }
        
        protected virtual IMessageQueueManager CreateQueueManager(IMessageQueue queue)
        {
            return new MessageQueueManager(queue);
        }
        
        [SetUp]
        public async void BeforeEach()
        {
            await this.ClearQueue();
        }

        protected virtual Task ClearQueue()
        {
            return Task.FromResult(false);
        }
    }
}


