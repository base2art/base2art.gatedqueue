﻿

namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading.Tasks;

    using Base2art.Tasks;
    using NUnit.Framework;

    [TestFixture]
    public class QueueTests : SimpleQueueTest
    {
        private readonly Lazy<string> ipAddress;
        
        public QueueTests()
        {
            this.ipAddress = new Lazy<string>(GetLocalIPAddress);
        }
        
        
        protected override bool ThrowErrors
        {
            get
            {
                return false;
            }
        }
        
        private string ConnectionString()
        {
            var builder = new global::MySql.Data.MySqlClient.MySqlConnectionStringBuilder();
            builder.UserID = "nunit";
            builder.Database = "nunit";
            builder.Password = "nunitUser!";
            builder.Server = ipAddress.Value == "10.0.2.15" ? "10.0.2.2" : "127.0.0.1";
            builder.Pooling = true;
            
            return builder.ToString();
        }
        
        protected override Task ClearQueue()
        {
            var connectionFactory = new MySqlConnectionFactory(ConnectionString());
            
            return base.ClearQueue()
                .Then()
                .Return(() => connectionFactory.Execute("TRUNCATE TABLE nunit.message_queue_message_v1;", null));
        }
        
        protected override ITypedMessageQueue CreateQueue(TimeSpan? delay = default(TimeSpan?))
        {
            var connectionString = ConnectionString();
            return new TypedMessageQueue(new MessageQueue(connectionString), new TestSerializer());
        }
        
        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
