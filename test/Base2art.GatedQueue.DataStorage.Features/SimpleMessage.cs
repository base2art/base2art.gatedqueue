﻿namespace Base2art.GatedQueue.DataStorage
{
    public class SimpleMessage
    {
        public string Data
        {
            get;
            set;
        }
    }
}
