﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Threading.Tasks;
    using FluentAssertions;
    using NUnit.Framework;

    public class SimpleQueueSpec
    {
        private readonly TimeSpan timeout = TimeSpan.FromSeconds(30);
        
        protected virtual ITypedMessageQueue CreateQueue(TimeSpan? delay = null)
        {
            return new IndexedMessageQueue.TypedMessageQueue(new InMemoryQueue.MessageQueue(), new TestSerializer()){ Delay = delay };
        }

        protected virtual bool ThrowErrors
        {
            get { return true; }
        }
        
        [SetUp]
        public async void BeforeEach()
        {
            await this.ClearQueue();
        }
        
//        [Test]
//        public async void ShouldOffer2Times()
//        {
//            ITypedMessageQueue queue = this.CreateQueue();
//            
//            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
//            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
//            
//            var found = await queue.Poll<SimpleMessage>();
//            found.ApplicationKey.Should().Be("abc");
//        }
        
//        [Test]
        
//        [Test]
//        public async void ShouldAcknowledgeExpired()
//        {
//            ITypedMessageQueue queue = this.CreateQueue();
//            
//            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, TimeSpan.FromSeconds(1));
//            var found = await queue.Poll<SimpleMessage>();
//            
//            await Task.Delay(TimeSpan.FromSeconds(1));
//            
//            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, TimeSpan.FromSeconds(1));
//            
//            var found1 = await queue.Poll<SimpleMessage>();
//            found1.ApplicationKey.Should().Be("abc");
//            found.Id.Should().NotBe(found1.Id);
//        }
        
        [Test]
        public async void ShouldLoad()
        {
            ITypedMessageQueue queue = this.CreateQueue();
            
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            var peeked = await queue.Peek<SimpleMessage>();
            peeked.Should().BeNull();
            
            var polled = await queue.Poll<SimpleMessage>();
            polled.Should().BeNull();
            
            var items = await queue.FindUnapproved(10);
            items.Length.Should().Be(1);
            
            
            var itemSet1 = await queue.FindUnapproved<SimpleMessage>(10);
            items.Length.Should().Be(1);
            itemSet1.Length.Should().Be(1);
            items[0].Id.Should().Be(itemSet1[0].Id);
            
            await queue.MarkApproved(items[0].Id, "system");
            
            var peekedAndFound = await queue.Peek<SimpleMessage>();
            peekedAndFound.Should().NotBeNull();
            
            var polledAndFound = await queue.Poll<SimpleMessage>();
            polledAndFound.Should().NotBeNull();
            polledAndFound.WorkItemData.Data.Should().Be("sjY");
            
            
            var peekedAndNotFound = await queue.Peek<SimpleMessage>();
            peekedAndNotFound.Should().BeNull();
        }
        
        
        
        [Test]
        public async void ShouldDoAsync()
        {
            ITypedMessageQueue queue = this.CreateQueue(TimeSpan.FromSeconds(1));
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            
            await Task.Delay(TimeSpan.FromSeconds(1));
            
            var found = await queue.FindUnapproved<SimpleMessage>(10);
            await queue.MarkApproved(found[0].Id, "system");
            
            var task1 = queue.Poll<SimpleMessage>();
            var task2 = queue.Poll<SimpleMessage>();
            
            var items = await Task.WhenAll(task1, task2);
            
            var item1 = items[0];
            var item2 = items[1];
            
            (item1 == null ^ item2 == null).Should().BeTrue();
            
            Guid? itemId1 = item1 != null ? (Guid?)item1.Id : null;
            Guid? itemId2 = item2 != null ? (Guid?)item2.Id : null;
            
            //            (item1 == item2).Should().BeTrue();
        }
        
        [Test]
        public async void ShouldDoubleMarkApproved_ThrowsException()
        {
            ITypedMessageQueue queue = this.CreateQueue();
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);

            await Task.Delay(TimeSpan.FromSeconds(1));
            
            var items = await queue.FindUnapproved(10);
            await queue.MarkApproved(items[0].Id, "system1");

            await Task.Delay(TimeSpan.FromSeconds(1));
            var message = await queue.Poll<SimpleMessage>();
            if (this.ThrowErrors)
            {
                Assert.ThrowsAsync<InvalidOperationException>(() => queue.MarkApproved(items[0].Id, "system2"));
            }
            else
            {
                await queue.MarkApproved(items[0].Id, "system2");
                //                await queue.MarkSuccessful(message.Id, "Testing");
                var data = await queue.GetMessage(message.Id);
            }
        }

        [Test]
        public async void ShouldComplete_Success()
        {
            ITypedMessageQueue queue = this.CreateQueue();
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);

            await Task.Delay(TimeSpan.FromSeconds(1));
            
            var items = await queue.FindUnapproved(10);
            await queue.MarkApproved(items[0].Id, "system");
            var message = await queue.Poll<SimpleMessage>();
            await queue.MarkSuccessful(message.Id);
            var message1 = await queue.Poll<SimpleMessage>();
            message1.Should().BeNull();
        }
        
        [Test]
        public async void ShouldComplete_Failed()
        {
            ITypedMessageQueue queue = this.CreateQueue();
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);

            var items = await queue.FindUnapproved(10);
            await queue.MarkApproved(items[0].Id, "system");
            var message = await queue.Poll<SimpleMessage>();
            await queue.MarkFailed(message.Id);
            var message1 = await queue.Poll<SimpleMessage>();
            message1.Should().BeNull();
        }

        protected virtual Task ClearQueue()
        {
            return Task.FromResult(true);
        }
    }
}
