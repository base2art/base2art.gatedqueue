﻿namespace Base2art.GatedQueue.DataStorage.Specs
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.Upgrade;
    using Base2art.GatedQueue.DataStorage.DbModels;
    using Base2art.GatedQueue.Models;
    using FluentAssertions;
    
    public class QueueManagementSpec
    {
        private readonly bool archive;

        public QueueManagementSpec(bool archive)
        {
            this.archive = archive;
        }
        
        protected virtual TimeSpan JobLength()
        {
            return TimeSpan.FromSeconds(1);
        }
        
        protected virtual Task TimeoutJob()
        {
            return Task.Delay(JobLength().Add(JobLength()));
        }
        
        protected virtual Task Delay()
        {
            return Task.FromResult(true);
        }

        protected virtual Task ClearQueue(IDataStore store)
        {
            return store.Delete<message_queue_message_v1>().Execute();
        }
        
        protected virtual IMessageQueue CreateQueue(IDataStore store)
        {
            return new DataStorage.MessageQueue(store);
        }

        protected virtual Task<IMessageQueueManager> Manager(IDataStore store)
        {
            return Task.FromResult<IMessageQueueManager>(
                this.archive
                ? (IMessageQueueManager)new ArchivingMessageQueueManager(store)
                : new DeletingMessageQueueManager(store));
        }

        protected virtual ITypedMessageQueue CreateTypedQueue(IMessageQueue queue)
        {
            return new TypedMessageQueue(queue, new TestSerializer());
        }

        protected async Task<ITypedMessageQueue> Queue(IDataStore store)
        {
            var queue = this.CreateQueue(store);
            await this.ClearQueue(store);
            return this.CreateTypedQueue(queue);
        }

        protected Task CreateTable(IDbms dbms, bool canDropTables)
        {
            return new Base2art.GatedQueue.DataStorage.DbModels.Actions.CreateTables_V1()
                .Execute(dbms, new DataDefinitionUpgradeProperties {
                             DbmsCanDropTables = canDropTables,
                             IsProduction = false
                         });
        }
        
        public async Task ShouldAcknowledgeExpired(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            ITypedMessageQueue queue = await this.Queue(store);
            IMessageQueueManager manager = await this.Manager(store);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage {
                                                             Data = "sjY"
                                                         }, "scott", 3, this.JobLength());
            var found = await queue.Poll<SimpleMessage>();
            found.RetryCount.Should().Be(3);
            await this.TimeoutJob();
            
            (await queue.Poll<SimpleMessage>()).Should().BeNull();
            await manager.SetupRetries();
            await this.Delay();
            var found_2 = await queue.Poll<SimpleMessage>();
            found_2.Should().NotBeNull()
                .And.Subject.As<ITypedMessage<SimpleMessage>>().RetryCount.Should().Be(2);
            (await queue.Poll<SimpleMessage>()).Should().BeNull();
            await this.TimeoutJob();
            await manager.SetupRetries();
            await this.Delay();
            (await queue.Poll<SimpleMessage>()).Should().NotBeNull()
                .And.Subject.As<ITypedMessage<SimpleMessage>>().RetryCount.Should().Be(1);
            (await queue.Poll<SimpleMessage>()).Should().BeNull();
            await this.TimeoutJob();
            await manager.SetupRetries();
            await this.Delay();
            (await queue.Poll<SimpleMessage>()).Should().NotBeNull()
                .And.Subject.As<ITypedMessage<SimpleMessage>>().RetryCount.Should().Be(0);
            (await queue.Poll<SimpleMessage>()).Should().BeNull();
            await this.TimeoutJob();
            await manager.SetupRetries();
            await this.Delay();
            (await queue.Poll<SimpleMessage>()).Should().BeNull();
        }
        
        public async Task ShouldTruncateCompleted_Success(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            ITypedMessageQueue queue = await this.Queue(store);
            IMessageQueueManager manager = await this.Manager(store);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage {
                                                             Data = "sjY"
                                                         }, "scott", 3, TimeSpan.FromSeconds(1));
            var found = await queue.Poll<SimpleMessage>();
            await this.Delay();
            (await manager.GetJobTypes()).Length.Should().Be(1);
            await queue.MarkSuccessful(found.Id);
            (await manager.GetJobTypes()).Length.Should().Be(1);
            await manager.TruncateCompleted();
            (await manager.GetJobTypes()).Length.Should().Be(0);
        }
        
        public async Task ShouldTruncateCompleted_Failure(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            ITypedMessageQueue queue = await this.Queue(store);
            IMessageQueueManager manager = await this.Manager(store);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage {
                                                             Data = "sjY"
                                                         }, "scott", 0, TimeSpan.FromSeconds(1));
            (await manager.GetJobTypes()).Length.Should().Be(1);
            var found = await queue.Poll<SimpleMessage>();
            await this.TimeoutJob();
            (await manager.GetJobTypes()).Length.Should().Be(1);
            await manager.TruncateCompleted();
            (await manager.GetJobTypes()).Length.Should().Be(1);
        }

        public async Task ShouldTruncateCompleted_Failure_Retries(IDbms dbms, IDataStore store, bool canDropTables, int queueExpected, int historyExpected)
        {
            await this.CreateTable(dbms, canDropTables);
            ITypedMessageQueue queue = await this.Queue(store);
            IMessageQueueManager manager = await this.Manager(store);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage {
                                                             Data = "sjY"
                                                         }, "scott", 0, this.JobLength());
            (await manager.GetJobTypes()).Length.Should().Be(1);
            var found = await queue.Poll<SimpleMessage>();
            await this.TimeoutJob();
            await this.TimeoutJob();
            (await manager.GetJobTypes()).Length.Should().Be(1);
            await manager.SetupRetries();
            await manager.TruncateCompleted();
            (await manager.GetJobTypes()).Length.Should().Be(queueExpected);
            //            (await manager.GetJobTypes()).Length.Should().Be(1);
        }
        
        public async Task ShouldGetQueueSize(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            ITypedMessageQueue queue = await this.Queue(store);
            IMessageQueueManager manager = await this.Manager(store);
            
            (await manager.GetQueueSize()).Should().Be(0);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage {
                                                             Data = "sjY"
                                                         }, "scott", 3, TimeSpan.FromSeconds(1));
            
            (await manager.GetQueueSize()).Should().Be(1);
            var found = await queue.Poll<SimpleMessage>();
            (await manager.GetQueueSize()).Should().Be(1);
            await this.Delay();
            (await manager.GetJobTypes()).Length.Should().Be(1);
            (await manager.GetQueueSize()).Should().Be(1);
            await queue.MarkSuccessful(found.Id);
            (await manager.GetQueueSize()).Should().Be(1);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage {
                                                             Data = "sjY"
                                                         }, "scott", 3, TimeSpan.FromSeconds(1));
            (await manager.GetQueueSize()).Should().Be(2);
            (await manager.GetJobTypes()).Length.Should().Be(1);
            (await manager.GetQueueSize()).Should().Be(2);
            await manager.TruncateCompleted();
            (await manager.GetQueueSize()).Should().Be(1);
            (await manager.GetJobTypes()).Length.Should().Be(1);
        }
        
        public async Task ShouldGetTypeCounts(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            ITypedMessageQueue queue = await this.Queue(store);
            IMessageQueueManager manager = await this.Manager(store);
            
            (await manager.GetQueueSize()).Should().Be(0);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage {
                                                             Data = "sjY"
                                                         }, "scott", 3, TimeSpan.FromSeconds(1));
            
            (await manager.GetQueueSize()).Should().Be(1);
            (await manager.GetJobTypeCounts()).Length.Should().Be(1);
            var found = await queue.Poll<SimpleMessage>();
            (await manager.GetQueueSize()).Should().Be(1);
            (await manager.GetJobTypeCounts()).Length.Should().Be(1);
            await this.Delay();
            (await manager.GetJobTypes()).Length.Should().Be(1);
            (await manager.GetQueueSize()).Should().Be(1);
            (await manager.GetJobTypeCounts()).Length.Should().Be(1);
            await queue.MarkSuccessful(found.Id);
            (await manager.GetQueueSize()).Should().Be(1);
            (await manager.GetJobTypeCounts()).Length.Should().Be(1);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage {
                                                             Data = "sjY"
                                                         }, "scott", 3, TimeSpan.FromSeconds(1));
            (await manager.GetQueueSize()).Should().Be(2);
            
            var typeCounts = (await manager.GetJobTypeCounts());
            typeCounts.Length.Should().Be(2);
            typeCounts[0].MessageType.Should().Be(typeof(SimpleMessage).FullName);
            typeCounts[0].Count.Should().Be(1);
            typeCounts[0].State.Should().Be(JobState.Queued);
            
            typeCounts[1].MessageType.Should().Be(typeof(SimpleMessage).FullName);
            typeCounts[1].Count.Should().Be(1);
            typeCounts[1].State.Should().Be(JobState.CompletedSuccessfully);
            
            (await manager.GetJobTypes()).Length.Should().Be(1);
            
            var found1 = await queue.Poll<SimpleMessage>();
            await queue.MarkSuccessful(found1.Id);
            (await manager.GetJobTypeCounts()).Length.Should().Be(1);
            
            (await manager.GetQueueSize()).Should().Be(2);
            await manager.TruncateCompleted();
            (await manager.GetQueueSize()).Should().Be(0);
            (await manager.GetJobTypeCounts()).Length.Should().Be(0);
            (await manager.GetJobTypes()).Length.Should().Be(0);
        }
    }
}
