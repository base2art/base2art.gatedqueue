﻿namespace Base2art.GatedQueue.DataStorage.Specs
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.Upgrade;
    using Base2art.GatedQueue.DataStorage.DbModels;
    using Base2art.GatedQueue.DataStorage.DbModels.Actions;
    using FluentAssertions;
    using NUnit.Framework;

    public class QueueSpec
    {
        private readonly TimeSpan timeout = TimeSpan.FromSeconds(30);
        
        protected virtual bool ThrowErrors
        {
            get { return false; }
        }

        protected Task CreateTable(IDbms dbms, bool canDropTables)
        {
            return new CreateTables_V1().Execute(dbms, new DataDefinitionUpgradeProperties{ DbmsCanDropTables = canDropTables, IsProduction = false});
        }
        
        protected virtual Task ClearQueue(IDataStore store)
        {
            return store.Delete<message_queue_message_v1>().Execute();
        }
        
        protected virtual IMessageQueue CreateQueue(IDataStore store)
        {
            return new DataStorage.MessageQueue(store);
        }

        protected virtual Task<IMessageQueueManager> Manager(IDataStore store)
        {
            return Task.FromResult<IMessageQueueManager>(new DeletingMessageQueueManager(store));
        }

        protected virtual ITypedMessageQueue CreateTypedQueue(IMessageQueue queue)
        {
            return new TypedMessageQueue(queue, new TestSerializer());
        }

        protected async Task<ITypedMessageQueue> Queue(IDataStore store)
        {
            var queue = this.CreateQueue(store);
            await this.ClearQueue(store);
            return this.CreateTypedQueue(queue);
        }
        
        protected virtual Task Delay()
        {
            return Task.FromResult(true);
        }
        
        public async Task ShouldOffer2Times(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            var queue = await this.Queue(store);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            
            var found = await queue.Poll<SimpleMessage>();
            found.ApplicationKey.Should().Be("abc");
        }
        
        public async Task ShouldOfferDuplicateApproved(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            var queue = await this.Queue(store);
            
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            await queue.OfferWithApproval<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            
            
            var items = await queue.FindUnapproved<SimpleMessage>(10);
            await queue.MarkApproved(items[0].Id, "system");
            
            var found = await queue.Poll<SimpleMessage>();
            found.ApplicationKey.Should().Be("abc");
        }
        
        public async Task ShouldLoad(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            var queue = await this.Queue(store);
            
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            var peeked = await queue.Peek<SimpleMessage>();
            peeked.Should().BeNull();
            
            var polled = await queue.Poll<SimpleMessage>();
            polled.Should().BeNull();
            
            var items = await queue.FindUnapproved(10);
            items.Length.Should().Be(1);
            
            
            var itemSet1 = await queue.FindUnapproved<SimpleMessage>(10);
            items.Length.Should().Be(1);
            itemSet1.Length.Should().Be(1);
            items[0].Id.Should().Be(itemSet1[0].Id);
            
            await queue.MarkApproved(items[0].Id, "system");
            
            var peekedAndFound = await queue.Peek<SimpleMessage>();
            peekedAndFound.Should().NotBeNull();
            
            var polledAndFound = await queue.Poll<SimpleMessage>();
            polledAndFound.Should().NotBeNull();
            polledAndFound.WorkItemData.Data.Should().Be("sjY");
            
            
            var peekedAndNotFound = await queue.Peek<SimpleMessage>();
            peekedAndNotFound.Should().BeNull();
        }
        
        public async Task ShouldDoAsync(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            // , TimeSpan.FromSeconds(1)
            var queue = await this.Queue(store);
            
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);
            
            await this.Delay();
            
            var found = await queue.FindUnapproved<SimpleMessage>(10);
            await queue.MarkApproved(found[0].Id, "system");
            
            var task1 = queue.Poll<SimpleMessage>();
            var task2 = queue.Poll<SimpleMessage>();
            
            var items = await Task.WhenAll(task1, task2);
            
            var item1 = items[0];
            var item2 = items[1];
            
            (item1 == null ^ item2 == null).Should().BeTrue();
            
            Guid? itemId1 = item1 != null ? (Guid?)item1.Id : null;
            Guid? itemId2 = item2 != null ? (Guid?)item2.Id : null;
        }
        
        public async Task ShouldDoubleMarkApproved_ThrowsException(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            var queue = await this.Queue(store);
            
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);

            await this.Delay();
            
            var items = await queue.FindUnapproved(10);
            await queue.MarkApproved(items[0].Id, "system1");

            await this.Delay();
            var message = await queue.Poll<SimpleMessage>();
            if (this.ThrowErrors)
            {
                Assert.ThrowsAsync<InvalidOperationException>(() => queue.MarkApproved(items[0].Id, "system2"));
            }
            else
            {
                await queue.MarkApproved(items[0].Id, "system2");
                //                await queue.MarkSuccessful(message.Id, "Testing");
                var data = await queue.GetMessage(message.Id);
            }
        }

        public async Task ShouldComplete_Success(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            var queue = await this.Queue(store);
            
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);

            await this.Delay();
            
            var items = await queue.FindUnapproved(10);
            await queue.MarkApproved(items[0].Id, "system");
            var message = await queue.Poll<SimpleMessage>();
            await queue.MarkSuccessful(message.Id);
            var message1 = await queue.Poll<SimpleMessage>();
            message1.Should().BeNull();
        }
        
        public async Task ShouldComplete_Failed(IDbms dbms, IDataStore store, bool canDropTables)
        {
            await this.CreateTable(dbms, canDropTables);
            var queue = await this.Queue(store);
            
            await queue.Offer<SimpleMessage>("abc", new SimpleMessage{ Data = "sjY" }, "scott", 3, timeout);

            var items = await queue.FindUnapproved(10);
            await queue.MarkApproved(items[0].Id, "system");
            var message = await queue.Poll<SimpleMessage>();
            await queue.MarkFailed(message.Id);
            var message1 = await queue.Poll<SimpleMessage>();
            message1.Should().BeNull();
        }
    }
}
