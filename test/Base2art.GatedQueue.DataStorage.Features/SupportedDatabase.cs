﻿namespace Base2art.GatedQueue.DataStorage
{
    public enum SupportedDatabase
    {
        MySql,
        InMemory,
        SqlServer,
        SqlCe
    }
}
