﻿namespace Base2art.GatedQueue.DataStorage
{
    using System;
    using System.Threading.Tasks;
    using Base2art.GatedQueue;

    public class TestSerializer : IObjectSerializationService
    {
        private static readonly Base2art.Serialization.NewtonsoftSerializer serializer = new Base2art.Serialization.NewtonsoftSerializer();
        public string SerializeObject(object data)
        {
            return serializer.Serialize(data);
        }
        
        public T DeserializeObject<T>(string data)
        {
            return serializer.Deserialize<T>(data);
        }
    }
    /*
    {
        string IObjectSerializationService.SerializeObject(object data)
        {
            return this.SerializeObjectFormatted(data);
        }
        

        public string SerializeObjectFormatted<T>(T data)
        {
            return JsonConvert.SerializeObject(data, Formatting.Indented);
        }

        public string SerializeObject<T>(T data)
        {
            return JsonConvert.SerializeObject(data);
        }

        public string SerializeObjectFormatted(object data, Type type)
        {
            return JsonConvert.SerializeObject(data, type, Formatting.Indented, new JsonSerializerSettings());
        }

        public string SerializeObject(object data, Type type)
        {
            return JsonConvert.SerializeObject(data, type, new JsonSerializerSettings());
        }

        public T DeserializeObject<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        public object DeserializeObject(string data, Type type)
        {
            return JsonConvert.DeserializeObject(data, type);
        }

        public Task<string> SerializeObjectFormattedAsync<T>(T data)
        {
            return Task.FromResult(JsonConvert.SerializeObject(data, Formatting.Indented));
        }

        public Task<string> SerializeObjectAsync<T>(T data)
        {
            return Task.FromResult(JsonConvert.SerializeObject(data));
        }

        public Task<string> SerializeObjectFormattedAsync(object data, Type type)
        {
            return Task.FromResult(JsonConvert.SerializeObject(data, type, Formatting.Indented, new JsonSerializerSettings()));
        }

        public Task<string> SerializeObjectAsync(object data, Type type)
        {
            return Task.FromResult(JsonConvert.SerializeObject(data, type, new JsonSerializerSettings()));
        }

        public Task<T> DeserializeObjectAsync<T>(string data)
        {
            return Task.FromResult(JsonConvert.DeserializeObject<T>(data));
        }

        public Task<object> DeserializeObjectAsync(string data, Type type)
        {
            return Task.FromResult(JsonConvert.DeserializeObject(data, type));
        }
    }*/
}


